### Exercicio 29

```
 Implemente um pacote capaz de realizar requisições para a API Pokeapi, cujo ponto principal pode ser acessado através do endereço: https://pokeapi.co/api/v2/pokemon/NUMERO. Utilize requsições http do tipo GET para extrair os dados (não esqueça de substituir o NUMERO por um número ounome). O script deverá retornar na tela para o usuário informações sobre o pokémon, como nome, tipos, peso (weight) e altura (height).

```

- Execute _npm run start_ para testar o resultado.
