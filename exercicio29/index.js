const axios = require('axios');

axios.get('https://pokeapi.co/api/v2/pokemon/94')
    .then(response => {
        const { name, height, weight, types } = response.data;

        const tipos = [];
        types.forEach(element => {
            tipos.push(element.type.name.capitalize());
        });
        console.log("Nome: ", name.capitalize());
        console.log("Altura: ", height);
        console.log("Peso: ", weight);
        console.log("Tipos:", tipos.reverse());
    })
    .catch(error => {
        console.log(error);
    });

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
}