const rl = require('readline-sync');
const fs = require('fs');

var p1, p2, p3, p4, p5;

p1 = rl.question("Digite a primeira palavra: ");
p2 = rl.question("Digite a segunda palavra: ");
p3 = rl.question("Digite a terceira palavra: ");
p4 = rl.question("Digite a quarta palavra: ");
p5 = rl.question("Digite a quinta palavra: ");

var total = [p1, p2, p3, p4, p5];
var array = [];

total.forEach(element => {
    if (element != '' && element != null) {
        if (!array.includes(element)) {
            array.push(element);
        }
    }
});

var json = JSON.stringify(array)

console.log("Conteudo a ser salvo: ", json);

fs.writeFileSync("data.json", json, { encoding: 'utf8', flag: 'w' });

console.log("Conteudo já foi gravado no arquivo data.json, favor checar o resultado.");

