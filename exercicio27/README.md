### Exercicio 27

```
Implemente um pacote capaz de solicitar ao usuário uma sequência de palavras.
Ao final, o programa deve salvar todas as palavras em um arquivo de texto (de preferência no formato JSON). No entanto, o arquivo não poderá conter palavras repetidas ou nulas.
```

- Execute _npm run start_ para começar a digitar as palavras.
