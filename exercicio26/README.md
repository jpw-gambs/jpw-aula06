### Exercicio 26

```
Crie um pacote usando npm e implemente um script que receba argumentos, como node index.js 1 2 3 4 5 6. O script deve retornar a soma total apenas dos argumentos divisíveis por 2.
```

- Execute _npm run start_ para testar o resultado.
