### Exercicio 28

```
 Implemente um pacote capaz de ler um arquivo de texto cujo o caminho é passado por parâmetro pelo usuário. O arquivo lido deverá conter palavras que serão persistidas no banco LevelDB.
```

> Para ler um arquivo execute:

- _npm run start_

> Para verificar se o conteudo do arquivo foi salvo no database local execute:

- _npm run read_
