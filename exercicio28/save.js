var rl = require('readline-sync');
var fs = require('fs');
var level = require('level');

console.log("O arquivo fornecido a seguir será lido e seu conteudo salvo no database LevelBD local.");

var caminho = rl.question("Digite o caminho do arquivo: ");

var meuArquivo = fs.readFileSync(caminho, "utf8");
var conteudo = JSON.stringify(meuArquivo);

console.log("conteudo", conteudo);

var db = level("28");
db.put("conteudo", conteudo, function (err) {
    if (!err) {
        console.log("Conteudo salvo com sucesso.");
    }
});
